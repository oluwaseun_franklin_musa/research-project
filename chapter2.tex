 \chapter{Preliminaries}
This chapter contains the definition and introduction to some terms and concepts that will be used in this project.

\section{Probability Theory}
\begin{defn}[Sigma Algebra]\citep{shreve2004stochastic}
A collection $\mathcal{F}$ of subsets of a non-empty set that satisfy the following properties:
\begin{enumerate}[(i)]
\item $\emptyset$ $\in$ $\mathcal{F}$.
\item If a set $A\in \mathcal{F}$, then $A^{c}\in \mathcal{F}$.
\item If a sequence of sets: $A_{1},A_{2},\dots,A_{n} \in \mathcal{F}$, then $\cup^n_{i=1} A_{i}\in \mathcal{F}$. 
\end{enumerate}
\end{defn}

\begin{defn}[Probability Measure]\citep{john1993options}
Let $\mathbb{P}$ be a probability measure on a probability space with sample space $\Omega$. Then,
\begin{enumerate}[(i)]
\item $0\leq \mathbb{P}(A)\leq 1\quad \forall \quad A\in \Omega$
\item $\mathbb{P}(\Omega)=1$
\item $\mathbb{P}\left(\cup_{i=1}^\infty A_i\right)=\sum_{i=1}^\infty \mathbb{P}(A_i)$
where $A_i,i=1,\dots$ are disjoint sets in $\Omega$.
\end{enumerate}

\end{defn}
\begin{defn}[Filtration]
A collection $(F_t,t\geq 0)$ of $\sigma$ -algebras on a sample space such that
\begin{align*}
\mathcal{F}_p \subseteq \mathcal{F}_q \quad\forall \quad 0\leq p\leq q.
\end{align*}
\end{defn}


\begin{defn}[Probability Density Function]
A function of a continuous variable whose integral over a region gives the probability that the random variable falls in that region.

A probability density function $f(x)$ satisfies the following properties:
\begin{enumerate}[(i)]
\item $f(x)\geq 0$ for all x.
\item $\int_{-\infty}^{\infty}f(x)dx=1$.
\end{enumerate}
\end{defn}

\begin{defn}[Cumulative Distribution Function]
The probability that a random variable X is less than or equal to a number x.
\end{defn}

\begin{defn}[Normal Distribution]
A continuous function that represents the distribution of random variables on a symmetric bell-shaped graphs.

The pdf for the Normal Distribution is given by:
\begin{align*}
f(x)=\frac{1}{\sigma\sqrt{2\pi}}e^{\frac{-1}{2}\frac{(x-\mu)^2}{\sigma^2}},
\end{align*}
where $\mu$ is its mean and $\sigma$ is its standard deviation.
\end{defn}

\begin{defn}[Log-Normal Distribution]
A random variable whose natural logarithm follows a Normal distribution. The probability density function of a Log-Normal distribution is given by:
\begin{align*}
f_{X}(x)=\frac{1}{\sigma x\sqrt{2\pi}}e^{-\frac{1}{2}\frac{(\ln x-\mu)^2}{\sigma^2}},
\end{align*}
where $\mu$ is the mean and $\sigma$ is the standard deviation of a normal random variable. The log-normal distribution curve is given below:
\end{defn}

\begin{figure}[h!] \centering
\includegraphics[width=0.5\textwidth]{/home/seun/research-project/sage0.png}
\caption{Log-normal distribution curve}
\label{Slope field}
\end{figure}

\section{Stochastic Process}
\begin{defn}[Stochastic Process]
A collection of random variables (indexed by time) defined on a probability space which assume values in $\mathbb{R}^n$.
\end{defn}
\begin{defn}[Standard Brownian Motion/Wiener Process]\citep{wiersema2008brownian}
A stochastic process $\{Z_t:0\leq t\leq\infty\}$ which satisfies the following properties:
\begin{enumerate}[(i)]
\item $Z_0=0$.
\item The process $\{Z_t\}_{t\geq 0}$ has stationary independent increments.
\item The increment $Z_{t+s}-Z_{s}$ is normally distributed with mean 0 and variance t.
\item $Z_t$ is continuous for all t.
\end{enumerate}
\end{defn}

\begin{defn}[Gaussian Process]\citep{shreve2004stochastic}
A stochastic process $Z(t), t\geq 0$ with the property that for arbitrary times $0<t_1<t_2<,\dots t_n$, the random variables $Z(t_1), Z(t_2),\dots, Z(t_n)$ are jointly normally distributed.
\end{defn}

\begin{defn}[Adapted Stochastic Process]
A stochastic process $Z=(Z_t,t\geq 0)$ is said to be adapted to the filtration $(\mathcal{F}_t,t\geq 0)$ if
\begin{align*}
\sigma(Z_t)\subseteq \mathcal{F}_t\quad \text{for all}\quad t\geq 0.
\end{align*}
\end{defn}

\begin{defn}[Martingale]\citep{shreve2004stochastic}
Let $X(t)$ be an adapted stochastic process in a filtered probability space. $X(t)$ is a martingale if:
\begin{enumerate}[(i)]
\item $E[X(t)]\leq \infty \quad \forall \quad t\geq 0$.
\item $E[X(t)|\mathcal{F}_s,t\geq s]=X(s)\quad \forall\quad 0\leq s\leq t\leq T$.
\end{enumerate}
\end{defn}

\section{Arithmetic Brownian Motion}
This is the name for a random process whose dynamics is given by:
\begin{align*}
dX(t)=adt+bdB(t),
\end{align*}
where the drift coefficient, a and the diffusion coefficient b are known constants and $b\geq 0$.

In integral form,
\begin{align*}
\int_{t=0}^TdX(t)=\int_{t=0}^Tadt+\int_{t=0}^T
bdB(t).
\end{align*}
This simplifies to
\begin{align*}
X(T)-X(0)&=a(T-0)+b[B(T)-B(0)],\\
\implies X(T)&=X(0)+aT+bB(T).
\end{align*}
The distribution parameters are:
\begin{align*}
E[X(T)]&=E[X(0)+aT+bB(T)],\\
&=X(0)+aT+bE[b(T)],\\
&=X(0)+aT.
\end{align*}
\begin{align*}
Var[X(T)]&=Var[X(0)+aT+bB(T)],\\
&=Var[bB(T)].
\end{align*}
The mean and variance are linear functions of time. This model is useful for an economic variable that grows at a constant rate and has a high level of uncertainty. However, it cannot be used to model stock prices as stock prices cannot go negative due to limited liability.

\section{Geometric Brownian Motion}
This is a model for the change in dynamics of a random process $dX(t)$ with the present value of $X(t)$. It is given by:
\begin{align*}
\frac{dX(t)}{X(t)}&=\alpha dt+\beta dB(t).\\
\implies dX(t)&=\alpha X(t)dt+\beta X(t)dB(t).
\end{align*}
where $\alpha$ represents the drift, $\beta$ represents the volatility and $B_t$ is the Weiner process.

A solution to the Geometric Brownian motion is gotten by using a trial solution and applying Ito's formula to get the corresponding stochastic differential equation. The usual trial solution is $\ln S(t)$ since the derivative for $\ln S(t)$ is $\frac{dS(t)}{S(t)}$.

For simplicity, setting $P=\ln S$,
\begin{align*}
\frac{dP}{dS}=\frac{1}{S}, \quad \frac{d^2P}{dS^2}=-\frac{1}{S^2}.
\end{align*}
By Ito's lemma,
\begin{align*}
dP&=\frac{dP}{dS}dS+\frac{1}{2}\frac{d^2P}{dS^2}(dS)^2,\\
\implies dP&=\frac{1}{S}\left(\alpha Sdt+\beta SdB(t)\right)-\frac{1}{2}\frac{1}{S^2}(\beta^2S^2dt),\\
dP&=\left(\alpha-\frac{\beta^2}{2}\right)dt+\beta dB(t).
\end{align*}
In integral form,
\begin{align*}
\int_{t=0}^TdP&=\int_{t=0}^T\left(\alpha-\frac{\beta^2}{2}\right)dt+\int_{t=0}^T\beta dB(t),\\
\implies \int_{t=0}^Td[\ln S(t)]&=\int_{t=0}^T\left(\alpha-\frac{\beta^2}{2}\right)dt+\int_{t=0}^T\beta dB(t),\\
\ln[S(T)]-\ln[S(0)]&=\left(\alpha-\frac{\beta^2}{2}\right)T+\beta B(T),\\
\ln\left[\frac{S(T)}{S(0)}\right]&=\left(\alpha-\frac{\beta^2}{2}\right) T+\beta B(T),\\
\implies S(T)&=S(0)\exp\left[\left(\alpha-\frac{\beta^2}{2}\right)T+\beta B(T)\right].
\end{align*}
Moreover, $\ln\left[\frac{S(T)}{S(0)}\right]$ is normally distributed with parameters:
\begin{align*}
E\left[\ln\left[\frac{S(T)}{S(0)}\right]\right]&=E\left[\left(\alpha-\frac{\beta^2}{2}\right)T+\beta B(T)\right],\\
&=\left(\alpha-\frac{\beta^2}{2}\right)T,
\end{align*}
and
\begin{align*}
\text{Var}\left(\ln\left[\frac{S(T)}{S(0)}\right]\right)&=\text{Var}\left[\left(\alpha-\frac{\beta^2}{2}\right)T+\beta B(T)\right],\\
&=\beta^2T.
\end{align*}

\section{Ito Process}
An Ito process is the general form of a Brownian motion with the parameters as functions of the underlying variable and time t. Suppose the parameters are a and b and the variable is X, then
\begin{align*}
a=a(X_t,t), \quad b=b(X_t,t).
\end{align*}
An n-dimensional Ito process is given by:
\begin{align}\label{eqq9}
X_t=X_0+\int_0^ta_sds + \int_0^tb_sdZ_s,
\end{align}
where Z is an m-dimensional standard Brownian motion, a is an n-dimensional $\mathcal{F}_t$ adapted process and b is an $(n\times m)$ dimensional $\mathcal{F}_t$ adapted process.

Equation \eqref{eqq9} can be written as:
\begin{align}
dX_t=a_tdt+b_tdZ_t.
\end{align}

An n-dimensional SDE  has the form:
\begin{align}
dX_t=a(X_t,t)dt+b(X_t,t)dB_t,\quad X_0=x,
\end{align}
where a is an n-dimensional adapted process, b is an $(n\times m)$ dimensional adapted process and $B_t$ is a Wiener process.

\section{Ito's Lemma}
If a variable Y follows the process
\begin{align*}
dY=a(Y,t)dt+b(Y,t)dZ,
\end{align*}
where dZ is a Wiener process, then a function F of Y and t follows the process.
\begin{align*}
dF=\left(\frac{\partial F}{\partial Y}a+\frac{\partial F}{\partial t}+\frac{1}{2}\frac{\partial^2F}{\partial Y^2}b^2\right)dt+\frac{\partial F}{\partial Y}bdZ.
\end{align*}

\section{Radon-Nikodym Derivative}
Let the probability density function of a Brownian motion at $Z(t)=x$ be given by:
\begin{align*}
\frac{1}{\sqrt{t}\sqrt{2\pi}}\exp\left[-\frac{1}{2}\left(\frac{x}{\sqrt{t}}\right)^2\right].
\end{align*}
This is the same as:
\begin{align*}
\frac{1}{\sqrt{t}\sqrt{2\pi}}\exp\left(-\frac{1}{2}\left(\frac{x}{\sqrt{t}}-m\right)^2\right)\quad \exp\left(-\frac{mx}{\sqrt{t}}+\frac{m^2}{2}\right).
\end{align*}
Putting $m=-\frac{k}{\sqrt{t}}$, where k is a constant, the expression becomes:
\begin{align*}
\frac{1}{\sqrt{t}\sqrt{2\pi}}\exp\left[-\frac{1}{2}\left(\frac{x+kt}{\sqrt{t}}\right)^2\right]\quad \exp\left[kx+\frac{k^2t}{2}\right].
\end{align*}
The above expression is a product of a probability density function for a normal distribution and a factor $\exp\left(kx+\frac{k^2t}{2}\right)$.

The ratio of the second probability density function to the original probability density function at x is:
\begin{align*}
\frac{1}{\sqrt{t}\sqrt{2\pi}}\exp\left[-\frac{1}{2}\left(\frac{x+kt}{\sqrt{t}}\right)^2\right]\bigg/ \frac{1}{\sqrt{t}\sqrt{2\pi}}\exp\left[-\frac{1}{2}\left(\frac{x}{\sqrt{t}}\right)^2\right].
\end{align*}
The expression $\exp\left[-kx-\frac{k^2t}{2}\right]$ is a random variable for different values of Z(t). The ratio is called the Radon-Nikodym derivative with respect to Z(t) \citep{wiersema2008brownian}.

\section*{Girsanov Transformation}
Given a random process
\begin{align*}
Z(t)=\exp\left[-kS(t)-\frac{1}{2}k^2t\right], \quad 0<t\leq T,
\end{align*}
where k is a constant. The starting point $Z(0)=\exp(0)=1$ and its values are non-negative. When Ito's formula is applied,
\begin{align*}
dZ(t)=-kZ(t)dS(t)
\end{align*}
which is driftless.

For any t, E[Z(t)]=1. A new probability $\mathbb{\hat{P}}$ can be created. Let a random process $\mathbb{\hat{S}}$ be a drift change to the Weiner process S for all t under probability measure $\mathbb{P}$,
\begin{align*}
\hat{S}(t)=S(t)+kt.
\end{align*}
Then, $\hat{B}(t)$ is a Weiner process under the new probability distribution created by Z from the original probability $\mathbb{P}$. This construction is referred to as the Girsanov transformation \citep{wiersema2008brownian}.
\section{Finance Terms}
\begin{defn}[Portfolio]
A collection of assets such as bonds, stocks and cash equivalents.
\end{defn} 

\begin{defn}[Option]
A contract that gives the buyer the right but not the obligation to buy or sell a particular asset at a price called the strike price on or before the expiry date.

The following factors affect the option prices:
\begin{enumerate}[(i)]
\item Asset price
\item Strike price
\item Volatility
\item Risk-free interest rate
\item Expected dividend.
\end{enumerate}
\end{defn}

\begin{defn}[Call Option]
A contract in which the holder (buyer) has the right but not the obligation to buy some security for a particular price (strike price) on or before the expiry date. The pay-off for a call option is max$(S(T)-K,0)$, where $S(T)$ is the spot price at expiration and K is the strike price.
\end{defn}
\begin{defn}[European Call Option]
An option for the right but not the obligation to buy a stock at a certain price (stock price) only at the expiry date.
\end{defn}
\begin{defn}[Volatility]
A measure of the uncertainty (standard deviation) associated with an investment.
\end{defn}
\begin{defn}[Strike Price] \citep{wiki:strikeprice}
The price at which the owner of an option can buy (call option) or sell (put option) an underlying security of commodity.
\end{defn}
\begin{defn}[Arbitrage]
The buying and selling of an asset at a higher price for profit.
\end{defn}
\begin{defn}[Stock Price]
The lowest amount a stock can be bought for.
\end{defn}
\begin{defn}[Payoff]
This is the maximum of the strike price subtracted from the stock price at the exercise period and zero. That is,
\begin{align*}
\text{max}\{S(T)-K,0\},
\end{align*}
where K is the strike price and S(T) is the stock price at time T. 
\end{defn}
The diagram for the pay-off of a call option is given below:
\begin{figure}[h!] \centering
\includegraphics[width=0.6\textwidth]{/home/seun/research-project/callpayoff.png}
\caption{Normal and log-normal distribution curve}
\label{Slope field}
\end{figure}
\begin{defn}[Risk-free rate]
The ratio of profit made in a riskless investment to the amount invested over a period of time.
\end{defn}


%\begin{thm}[Jeff's Washing Theorem]
%\label{thm:jwt}
%If an item of clothing is too big, then washing it makes it bigger;
%\end{thm}



%\begin{proof}

%Stated without proof. But a proof would look like this. 







%Notice that no Lemmas are required in the proof of Theorem \ref{thm:jwt}.

%Use \textbackslash ref for tables, figures, theorems, etc. and \textbackslash eqref for equations.

%Use \textbackslash ldots for continuation of commas $,\ldots,$ and \textbackslash cdots for continuation of operators $\times\cdots\times$.
