set table "master-document.exp.table"; set format "%.5f"
set samples 1000; plot [x=0:3.0] (2/(x*0.4*sqrt(2*pi)))*exp(-(((log(x)-0.1)**2))/(2*0.4**2))
