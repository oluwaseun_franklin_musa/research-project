\chapter{Pricing a European Call Option with the Stein-Stein model}
Stochastic volatility models have the following general form:
\begin{align*}
dS_t&=\alpha_tS_tdt+f(\sigma_t,S_t)dZ_t^s\quad \text{and}\\
d\sigma_t&=g(\sigma_t,t)dt+h(\sigma_t,t)dZ_t^\sigma 
\end{align*}
with
\begin{align*}
dZ_t^sdZ_t^\sigma=\rho dt
\end{align*}
where the Weiner processes for the stock price and volatility, $Z_t^s$ and $Z_t^\sigma$ respectively have a correlation of $\rho$.

\section{Statement of the problem}
The underlying stock price of a European contingent claim is given by the following stochastic volatility model:

%\begin{thm}[Jeff's Washing Theorem]
%\label{thm:jwt}
%If an item of clothing is too big, then washing it makes it bigger;
%\end{thm}
%\begin{proof}
%Stated without proof. But a proof would look like this. 
%\end{proof}
\begin{align}\label{eqq8}
dS_1(t)&=\alpha S(t)dt+\sigma(t)S(t)dB_1(t),\quad S(0)=s_1\\ \label{eqq9}
d\sigma(t)&=\kappa^*(\nu^*-\sigma(t))dt+\theta dB_2(t)
\end{align}
where
\begin{enumerate}[(i)]
\item $\alpha$ is the drift of the process for the stock,
\item $\theta>0$ is the volatility of the variance, 
\item $\kappa^*>0$ is the mean reversion speed for variance,
\item $\nu^*>0$ is the mean reversion level for the variance,
\item $\sigma_0$ is the initial level of variance and 
\item $dB_1(t)$ and $dB_2(t)$ are two independent Brownian motions.
\end{enumerate}
The goal of this research is to determine if a closed form solution for the option price exists and summarize how to obtain it if it exists. I will also use some numerical methods to obtain the option price for some chosen parameters and compare this result to that obtained in \citet{stein1991stock}. However, the first step is to derive a partial differential equation for the model and specify the appropriate boundary conditions; this is seen in the following sections.


\section{Change of measure}
In order to get the partial differential equation, the Girsanov change of probability measure is applied to get a new probability distribution.

The risk neutral process for the stock price is:
\begin{align*}
dS_t=rS(t)dt+\sigma(t)d\tilde{B}_1(t)
\end{align*}
where 
\begin{align*}
\tilde{B_1}(t)=\left(B_1(t)+\frac{\alpha -r}{\sigma(t)}t\right)
\end{align*}
If the stock pays a continuous dividend q, r is replaced by r-q.

For a risk-neutral process, the function $\lambda(S,\sigma,t)$ is introduced into the drift of $d\sigma(t)$.
\begin{align*}
d\sigma(t)=[\kappa^*(\nu^*-\sigma(t))-\lambda(S(t),\sigma(t),t]dt+\theta d\tilde{B_2}(t)
\end{align*}
where
\begin{align*}
\tilde{B_2}(t)=\left(B_2(t)+\frac{\lambda(S(t),\sigma(t),t)}{\theta}t\right) 
\end{align*}
The function $\lambda(S,\sigma,t)$ is the volatility risk premium. This premium is a linear function of the volatility of volatility.
Substituting for $\lambda(S,\sigma,t)$,
\begin{align}
d\sigma(t)=\kappa(\nu-\sigma(t))dt+\theta d\tilde{B_2}(t)
\end{align}
where 
\begin{align*}
\kappa=\kappa^*+\lambda \quad \text{and}\quad \nu=\frac{\kappa^*\theta}{\kappa^*+\lambda}
\end{align*}
are the risk neutral parameters of the variance process.
The above model is the Stein-Stein model.

Hence, the risk-neutral process is
\begin{align}
dS(t)&=r S(t)dt+\sigma(t)S(t)\tilde{dB_1(t)},\quad S(0)=s_1\\
d\sigma(t)&=\kappa(\nu-\sigma(t))dt+\theta \tilde{dB_2(t)}
\end{align}

\section{Portfolio Setup}
Forming a self-financing portfolio with one option $V=V(S_1,\sigma,t)$, $\Delta$ units of stock S and $\phi$ units of another option $U=U(S,\sigma,t)$ that is used to hedge the volatility, the value of the portfolio is 
\begin{align}
\Pi=V+\Delta S+\phi U
\end{align}
where $\Pi=\Pi_t$. Suppose the portfolio is self-financing, the change in portfolio value is
\begin{align}
d\Pi=dV+\Delta dS+\phi dU
\end{align}



\section{Portfolio Dynamics}
Applying Ito's formula for the stochastic differentials of the options,
\begin{align}
dV=\frac{\partial V}{\partial t}dt+\frac{\partial V}{\partial S_1}dS+\frac{\partial V}{\partial\sigma}d\sigma+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}dt+\frac{1}{2}\theta^2\frac{\partial^2 V}{\partial\sigma^2}dt
\end{align}
Similarly,
\begin{align}
dU=\frac{\partial U}{\partial t}dt+\frac{\partial U}{\partial S}dS+\frac{\partial U}{\partial\sigma}d\sigma+\frac{1}{2}\sigma^2S^2\frac{\partial^2U}{\partial S^2}dt+\frac{1}{2}\theta^2\frac{\partial^2 U}{\partial\sigma^2}dt
\end{align}
Now, 
\begin{align}\label{eqq1}
\begin{split}
d\Pi=\left(\frac{\partial V}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2V}{\partial\sigma^2}\right)dt+
\phi\left(\frac{\partial U}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2U}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2U}{\partial\sigma^2}\right)dt+\\
\left(\frac{\partial V}{\partial S}+\phi\frac{\partial U}{\partial S_1}+\Delta\right)dS+\left(\frac{\partial V}{\partial\sigma}+\phi\frac{\partial U}{\partial\sigma}\right)d\sigma
\end{split}
\end{align}

\section{The Riskless Portfolio}
The last two terms of equation \eqref{eqq1} must be zero in order for the portfolio to be hedged against movements in the stock and volatility.
\begin{align*}
\implies \phi=-\frac{\frac{\partial V}{\partial\sigma}}{\frac{\partial U}{\partial\sigma}},\quad \Delta=-\phi\frac{\partial U}{\partial S}-\frac{\partial V}{\partial S}
\end{align*}
Substituting the values of $\phi$ and $\Delta$,
\begin{align*}
d\Pi=\left(\frac{\partial V}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2V}{\partial\sigma^2}\right)dt+
\phi\left(\frac{\partial U}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2U}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2U}{\partial\sigma^2}\right)dt
\end{align*}
The condition that the portfolio earns risk-free rate r, implies that the change in portfolio value is $d\Pi=r\Pi~dt$.
\begin{align}
\implies d\Pi=r(V+\Delta S+\phi U)dt
\end{align}
Equating the $d\Pi$'s,
\begin{align*}
\frac{\left(\frac{\partial V}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2V}{\partial\sigma^2}\right)-rV+rS\frac{\partial V}{\partial S}}{\frac{\partial V}{\partial\sigma}}
=\frac{\left(\frac{\partial U}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2U}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2U}{\partial\sigma^2}\right)-rU+rS\frac{\partial U}{\partial S}}{\frac{\partial U}{\partial\sigma}}
\end{align*}
\section{PDE for the Option Price}
The LHS and RHS of the above equation are functions of only V and only U respectively. So, the RHS can be written as:
\begin{align}
f(S,\sigma,t)&=-\kappa(v-\sigma)+\lambda(S,\sigma,t)
\end{align}
\begin{align}
\frac{\left(\frac{\partial V}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2V}{\partial\sigma^2}\right)-rV+rS\frac{\partial V}{\partial S}}{\frac{\partial V}{\partial\sigma}}= -\kappa(v-\sigma)+\lambda(S,\sigma,t)\\
\frac{\partial V}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2V}{\partial\sigma^2}-rV+rS\frac{\partial V}{\partial S}+[\kappa(v-\sigma)-\lambda(S,\sigma,t)]\frac{\partial V}{\partial\sigma}=0
\end{align}
The risk free rate, $\lambda$ is a linear function of the volatility of volatility. So, the PDE can be expressed as:
\begin{align}\label{eq9}
\frac{\partial V}{\partial t}+\frac{1}{2}\sigma^2S^2\frac{\partial^2V}{\partial S^2}+\frac{1}{2}\theta^2\frac{\partial^2V}{\partial\sigma^2}-rV+rS\frac{\partial V}{\partial S}+[\kappa(v-\sigma)-\lambda\theta]\frac{\partial V}{\partial\sigma}=0
\end{align}

At maturity, the call is worth its intrinsic value. So, the option price
\begin{align*}
V(S,\sigma,T)=\text{max}(S-K,0).
\end{align*}
When the stock price is zero, the call is worthless. Delta approaches 1 as the stock price increases and when volatility increases, the call option becomes equal to the stock price. This implies the following boundary conditions:
\begin{align}
V(0,\sigma,t)=0,\quad
\frac{\partial V}{\partial S}(\infty,\sigma,t)=1,\quad
V(S,\infty,t)=S
\end{align}

\section*{Closed form solution for Stock price}
For normalization, let the initial stock price $S(0)=0$. The following are variable definitions.
\begin{align*}
P=-\frac{\kappa}{\theta^2},\quad Q=\frac{\nu\kappa}{\theta^2},\quad R=-\frac{\omega}{\theta^2t}
\end{align*}
The following are definitions of some integral expressions:
\begin{align*}
a\equiv\sqrt{P^2-2R},\quad b\equiv -\frac{P}{a},\quad
L\equiv -P-a\left(\frac{\sinh(a\theta^2t)+b\cosh(a\theta^2t)}{\cosh(a\theta^2t)+b\sinh(a\theta^2t)}\right)
\end{align*}
\begin{align*}
M\equiv Q\left\{\frac{b\sinh(a\theta^2t)+b^2\cosh(a\theta^2t)+1-b^2}{\cosh(a\theta^2t)+b\sinh(a\theta^2t)}-1\right\}
\end{align*}
\begin{align*}
\begin{split}
N=\frac{a-P}{2a^2}[a^2-PQ^2-Q^2a]\theta^2t+\frac{Q^2[P^2-a^2]}{2a^3}\times \left\{\frac{(2Q+a)+(2Q-a)e^{2a\theta^2t}}{(Q+a+(a-Q)e^{2a\theta^2t})}\right\}\\
+\frac{2PR^2[a^2-P^2]e^{a\theta^2t}}{a^3(P+a+(a-P)e^{2a\theta^2t})}-\frac{1}{2}\ln\left\{\frac{1}{2}\left(\frac{P}{a}+1\right)+\frac{1}{2}\left(1-\frac{P}{a}\right)e^{2a\theta^2t}\right\}
\end{split}
\end{align*}

Let $\sigma$ be fixed and $\alpha=0$. A solution to equation  \eqref{eqq8} gives prices whose distribution is lognormal, $L(\sigma)$.
\begin{align*}
L(\sigma)=\sqrt{2\pi t\sigma^2 S^2}~\exp\left(\frac{-(\ln S+t\sigma^2/2)^2}{2t\sigma^2}\right).
\end{align*}
If $\sigma$ is a deterministic function $\sigma(t)$, the prices follow the distribution $L(\beta(t))$ where 
\begin{align*}
\beta(t)=\left(\frac{1}{t}\int_0^t \sigma^2(u) du\right)^{\frac{1}{2}}.
\end{align*}
The stock prices still follow a lognormal distribution, where the variance can be seen as the average $\sigma^2$ over the time interval.

When $\sigma(t)$ is stochastic as in \eqref{eqq9}, where it follows an Orstein-Uhlenbeck process (AR1 process), $\sigma=\sigma_{x}(t)$ is a point that signifies the stochastic path in the probability space. It follows that each path x gives a distribution for price $L(\beta_x(t))$, where
\begin{align*}
\beta_{x}(t)=\left(\frac{1}{t}\int_0^t\sigma_w^2(s)~ds\right)^{\frac{1}{2}}.
\end{align*}
It turns out that the distribution for the stock price S, is the expectation of $L(\beta_{x}(t))$ so that 
\begin{align}\label{eqq5}
S=E_{x}\{L(\beta_x(t))\}.
\end{align}

For the random variable $\beta_x(t)$, let $m_t(\sigma)d\sigma$ be its probability density function so that
\begin{align*}
P_w\{a<\beta_x(t)<b\}=\int_a^b m_t(\beta)~d\beta.
\end{align*}
So, for any function G,
\begin{align*}
E_x(G(\beta_x(t)))=\int G(\sigma)m_t(\sigma)~d\sigma.
\end{align*}
Hence, equation \eqref{eqq5} implies that
\begin{align*}
S=\int L(\sigma)m_t(\sigma)d\sigma,
\end{align*}
where $m_t(\sigma)$ is a mixing distribution.

\section{Lemma}
For all $\lambda\geq0$, $I(\lambda)$ is given by 
\begin{align*}
I=\exp(A\sigma_0^2+B\sigma_0+C)
\end{align*}
The exact formula for the stock price can be gotten with the lemma above. By using Fourier transform and inversion techniques,
\begin{align}\label{eq1}
g(y)=\int_{-\infty}^{\infty}e^{ixy}f(x)dx,\\ \label{eq2}
f(x)=\frac{1}{2\pi}\int_{-\infty}^{\infty}e^{-ixy}g(y)dy
\end{align}
Let $f(x)=S\cdot P(S,t)$. By the change of variable $x=\ln S$,
\begin{align*}
f(x)=\int \frac{1}{\sqrt{(2\pi t\sigma^2)}}\exp\left(\frac{-(x+t\sigma^2/2)^2}{2t\sigma^2}\right)m(\sigma)~d\sigma.
\end{align*}
Next, applying equation \eqref{eq1},
\begin{align}\label{eq2}
g(y)=\int m(\sigma)\left\{\int \frac{1}{\sqrt{2\pi t\sigma^2}}\times \exp\left(\frac{-(x+t\sigma^2/2)^2}{2t\sigma^2}\right)e^{ixy}dx\right\}d\sigma.
\end{align}
However, 
\begin{align*}
I(\omega) =\int_0^\infty e^{-\omega\sigma^2}m(\sigma)d\sigma.
\end{align*}
So, equation \eqref{eq2} can be expressed as
\begin{align}\label{eq3}
g(y)=I((y^2+iy)t/2).
\end{align}
Next, applying the Fourier inversion formula in equation \eqref{eq2} to equation \eqref{eq3},
\begin{align}\label{eq4}
f(x)=\frac{1}{2\pi}\int e^{-ixy}I\left((y+iy)\frac{t}{2}\right)dy.
\end{align}
The change of variable $y=q-i/2$ is made in equation \eqref{eq4} and the necessary shift of contour is done. Since $y^2+iy=q^2+\frac{1}{4}$,
\begin{align}\label{eq5}
f(x)=\frac{1}{2\pi}e^{-x/2}\int e^{-ixq}I\left(\left(q^2+\frac{1}{4}\right)\frac{t}{2}\right)dq.
\end{align}
From the definitions $f(x)=S.P_0(S,t)$ and $x=\ln S$, equation \eqref{eq5} becomes
\begin{align*}
P_0(S,t)=\frac{1}{2\pi}S^{-3/2}\times \int_{q=-\infty}^{\infty} I\left(\left(q^2+\frac{1}{4}\right)\frac{t}{2}\right)e^{iq\ln S}dq
\end{align*}
For a more general case $(\alpha\neq 0)$, the probability distribution for the stock price at time t is given by
\begin{align*}
P(S,t)=e^{-\alpha t}P_0(Se^{-\alpha t}).
\end{align*}

\section*{Option Pricing}
Assuming risk neutrality and taking $\lambda=0$, the option price for a European call is given by
\begin{align*}
V_0=e^{-rt}\int_{S=K}^\infty [S-K]~ P(S,t|\kappa,r,\theta,\nu)~dS.
\end{align*}
The subscript 0 on V indicates that $\lambda=0$. The probability distribution of the stock price P(S,t) is generated with the assumption that the stock's drift is equal to the riskless rate r. The mean of P(S,t) is equal to $S_0e^{rt}$.

For the case when $\lambda\neq 0$,
\begin{align*}
V=e^{-rt}\int_{S=K}^\infty [S-K]~P(S,t|\kappa,r,\theta,\tilde{\nu}),
\end{align*}
where $\tilde{\nu}=\nu-\frac{\lambda\theta}{\kappa}$.
%Notice that no Lemmas are required in the proof of Theorem \ref{thm:jwt}.

%Use \textbackslash ref for tables, figures, theorems, etc. and \textbackslash eqref for equations.

%Use \textbackslash ldots for continuation of commas $,\ldots,$ and \textbackslash cdots for continuation of operators $\times\cdots\times$.

%\lipsum[1]


%\begin{thm}[My Theorem2]
%This is my theorem2.
%\end{thm}
%\begin{proof}
%And it has no proof2.
%\end{proof}

%Lorum ipsum.



%+V_{i,j+1}\left[\frac{1}{2}\frac{\theta^2}{(\Delta\sigma)^2}+\frac{[\kappa(v-\sigma(t))]}{2\Deta\sigma}+\frac{ri}{2}\right \Delta t+V^n_{i-1,j}\left[\frac{1}{2}j^2i\frac{(\Delta\sigma)^2}{\Delta S_1}\right]\Delta t+V^n_{i,j-1}\left[\frac{1}{2}\frac{\theta^2}{(\Delta\sigma)^2}-\frac{ri}{2}-\frac{\kappa(v-\sigma(t))}{2\Delta\sigma}\right]\Delta t

%\lipsum[1]

%\begin{thm}[My Theorem2]
%This is my theorem2.
%\end{thm}
%\begin{proof}
%And it has no proof2.
%\end{proof}

%\lipsum[1]

%\begin{align} % do not use eqnarray. 
%\label{2ya}
%x & = y + y\\
%\label{2yb}
%& = 2y
%\end{align}
%Equations \eqref{2ya} and \eqref{2yb} are trivial.

%\section{Numbering in AIMS essays}

%Here is a conjecture:

%\begin{conj}
%The washing operation has fixed points.
%\end{conj}
%
%And here is an example:

%\begin{exa}
%5 Rand coin.
%\end{exa}

%\subsection{This is a subsection}

%\lipsum[1]

%\section{This is a section}

%\lipsum[1]



